from django.contrib import admin
from django.conf.urls import include, url

urlpatterns = [
    url('redditSparkles/', include('redditSparkles.urls')),
    url('admin/', admin.site.urls)
]

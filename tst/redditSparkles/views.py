from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

# Spark Context Usage
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql.functions import desc

conf = SparkConf().setAppName('Dataset').setMaster('local')
sc = SparkContext(conf=conf)

sql_sc = SQLContext( sc )


df_cmts = sql_sc.read.format('csv').options(header='false', inferSchema='true').load('redditSparkles/dataset/cmts.csv')
df_cmts = df_cmts.toDF( 'id', 'subreddit', 'authorName',
    'body','distinguished', 'edited',
    'locked', 'stickied', 'score'
)

df_sub = sql_sc.read.format('csv').options(header='false', inferSchema='true').load('redditSparkles/dataset/subms.csv')
df_sub = df_sub.toDF(
    'id', 'subreddit', 'authorName', 'title', 'name',
    'selftext', 'distinguished', 'edited', 'locked', 'over_18',
    'stickied', 'score', 'numComments', 'upvoteRatio', 'Source'
)




def index(request):
    from termcolor import colored
    print( colored( 'view test', 'red' ) )

    return render( request, 'redditSparkles/index.html' )



def top_subms( request, n ) :
    n = int(n)
    res = df_sub.sort( desc( 'score' ) ).select( 'score', 'subreddit' ).take( n )
    
    # results = {}
    # for i in range( n ) :
    #     results[ i ] = { 'score' : res[i]['score'], 'subreddit' : res[i]['subreddit'] }
    
    results = []
    for i in range( n ) :
        results.append({ 'score' : res[i]['score'], 'subreddit' : res[i]['subreddit'] })
    
    context = { "results" : results }
    print( context )

    #return HttpResponse( df_sub.sort( desc( 'score' ) ).select( 'score', 'subreddit' ).take( n ) )
    return  render( request, 'redditSparkles/top_subms.html', context)



def test( request ) :
    return HttpResponse( 'here 1 ' )
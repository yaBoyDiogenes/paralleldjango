from django.apps import AppConfig


class RedditsparklesConfig(AppConfig):
    name = 'redditSparkles'

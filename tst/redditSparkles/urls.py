from django.conf.urls import include, url

from . import views

app_name = 'redditSparkles'
urlpatterns = [
    url( r'^$', views.index, name='index' ),
    #url( r'top_subms/([0-9]+)/$', views.top_subms, name='top_subms' ),
    url( r'top_subms/(?P<n>[0-9]+)/$', views.top_subms, name='top_subms' ),
    url( r'^test', views.test, name='test' )
]
